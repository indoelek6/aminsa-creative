const Footer = ({isSection}) => {
    return (
        <> 
        {isSection &&
            <div className="absolute top-[3067px] left-[0px] bg-darkslateblue-200 w-full h-[222px] overflow-hidden text-9xl">
                <b className="absolute top-[55px] left-[calc(50%_-_371px)] leading-[140%]">
                    Need help marketing? We'd love to hear from you.
                </b>
                <div className="absolute top-[124px] left-[calc(50%_-_208px)] flex flex-row items-start justify-start gap-[15px] text-center text-base text-darkgray">
                    <div className="relative w-[302px] h-[43px]">
                        <div className="absolute top-[0px] left-[0px] rounded-[20px] bg-white w-[302px] h-[43px]" />
                        <input className="absolute top-[6px] left-[15px] placeholder:leading-[140%] text-sm w-[220px] h-7" placeholder="enter your email"/>
                    </div>
                    <div className="rounded-3xl bg-white hover:bg-transparent border-[2px] border-solid hover:border-white w-[100px] h-[43px] flex flex-row p-[13px] box-border items-start justify-start text-xl text-darkslateblue-100 hover:text-white">
                        <div className="self-stretch flex-1 relative leading-[140%] font-medium flex items-center justify-center">
                            Start
                        </div>
                    </div>
                </div>
            </div>
        }
            <div className="absolute top-[3289px] left-[0px] bg-darkslateblue-100 w-full h-[433px] overflow-hidden text-base">
                <div className="absolute top-[176px] left-[150px] text-xs leading-[140%] font-medium inline-block w-[255px]">
                    <p className="m-0">WTC Mall Matahari Serpong TA7-9,</p>
                    <p className="m-0">Jalan Raya Serpong,</p>
                    <p className="m-0">Kota Tangerang Selatan.</p>
                </div>
                <div className="absolute top-[141px] left-[435px] leading-[140%] font-medium">
                    0818 0567 2600
                </div>
                <div className="absolute top-[141px] left-[735px] flex flex-col items-start justify-start gap-[15px]">
                    <div className="relative leading-[140%] font-medium">Branding</div>
                    <div className="relative leading-[140%] font-medium">
                    Product Photography
                    </div>
                    <div className="relative leading-[140%] font-medium">
                    Social Media Management
                    </div>
                </div>
                <div className="absolute top-[176px] left-[435px] leading-[140%] font-medium">
                    info@cahayaamerta.com
                </div>
                <div className="absolute top-[141px] left-[150px] text-xs leading-[140%] font-medium">
                    PT. CAHAYA AMERTA INTERNUSA
                </div>
                <b className="absolute top-[88px] left-[435px] text-xl leading-[140%]">
                    Contact
                </b>
                <b className="absolute top-[88px] left-[735px] text-xl leading-[140%]">
                    Services
                </b>
                <b className="absolute top-[88px] left-[1055px] text-xl leading-[140%]">
                    Social Media
                </b>
                <img
                    className="absolute top-[85.95px] left-[150px] w-[150px] h-[30.06px]"
                    alt=""
                    src="/logo-aminsa.svg"
                />
                <div className="absolute top-[141px] left-[1055px] w-[140px] h-10">
                    <div className="absolute top-[0px] left-[0px] rounded-11xl bg-white flex flex-col p-2.5 items-start justify-start">
                    <img className="relative w-5 h-5" alt="" src="/vector.svg" />
                    </div>
                    <div className="absolute top-[0px] left-[50px] rounded-11xl bg-white flex flex-col p-2.5 items-start justify-start">
                    <img className="relative w-5 h-5" alt="" src="/vector1.svg" />
                    </div>
                    <div className="absolute top-[0px] left-[100px] rounded-11xl bg-white flex flex-col p-2.5 items-start justify-start">
                    <img
                        className="relative w-5 h-[19.88px]"
                        alt=""
                        src="/vector2.svg"
                    />
                    </div>
                </div>
                <div className="absolute top-[286px] w-full flex flex-col items-start justify-start">
                    <img
                    className="self-stretch relative max-w-full overflow-hidden h-0.5 shrink-0"
                    alt=""
                    src="/vector-4.svg"
                    />
                </div>
                <div className="absolute top-[336px] left-[calc(50%_-_254px)] leading-[140%] font-medium">
                    © Copyright 2023 Aminsa Creative House | All Rights Reserved.
                </div>
                </div>
                <div className="absolute top-[20px] left-[calc(50%_-_640px)] bg-darkslateblue-100 w-full" />
                <div className="absolute top-[0px] right-[0px] w-full h-[720px] text-6xl">
                <div className="absolute top-[0px] bg-darkslateblue-100 w-full h-[720px]" />
                <img
                    className="absolute top-[0px] w-full h-[513px]"
                    alt=""
                    src="/img-overlay.svg"
                />
                <div className="absolute top-[0px] right-[0px] rounded-tl-[355px] rounded-tr-[355.5px] rounded-b-none bg-salmon w-[539px] h-[720px]" />
                <div className="absolute top-[212px] left-[100px] w-[590px] h-[345px]">
                    <div className="absolute top-[275px] left-[0px] leading-[140%] flex items-center w-[517px]">
                    optimistic, inspiring, and expressive, cheerful, positive and
                    charming.
                    </div>
                    <div className="absolute top-[0px] left-[3px] w-[587px] h-[255px] text-[61px]">
                    <b className="absolute top-[0px] left-[0px] leading-[140%] flex items-center w-[587px]">
                        <span className="[line-break:anywhere] w-full">
                        <p className="m-0">We Are</p>
                        <p className="m-0">
                            <span>The</span>
                            <span className="text-orange"> Light</span>
                        </p>
                        <p className="m-0">In Every Aspect,</p>
                        </span>
                    </b>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Footer