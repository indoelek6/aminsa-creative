const Footer_2 = ({contact}) => {
    return (
        <div className="w-full ">
            {contact &&
            <div className="left-[0px] content-center justify-center items-center flex flex-row bg-darkslateblue-200 w-full h-[222px] overflow-hidden text-9xl">
                <div>
                    <b className="flex flex-row  items-start justify-center text-center leading-[140%] text-white">
                        Need help marketing? We'd love to hear from you.
                    </b>
                    <div className="flex flex-row mt-5  items-start justify-center gap-[15px] text-center text-base text-darkgray">
                        <div className="w-[302px] h-[43px]">
                            <div className="left-[0px] rounded-[20px] bg-white w-[302px] h-[43px]">
                                <input className=" mt-[6px] mr-10 placeholder:leading-[140%] text-sm w-[220px] h-7" placeholder="enter your email"/>
                            </div>
                        </div>
                        <div className="rounded-3xl bg-white hover:bg-transparent border-[2px] border-solid hover:border-white w-[100px] h-[43px] flex flex-row p-[13px] box-border items-start justify-start text-xl text-darkslateblue-100 hover:text-white">
                            <div className="self-stretch flex-1 leading-[140%] font-medium flex items-center justify-center">
                                Start
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            }
            <div className="bg-darkslateblue-100 text-white content-center w-full h-[433px] overflow-hidden text-base">
                <div className="justify-center items-center flex mt-24">
                    <div className="justify-center gap-5 grid grid-cols-4  flex-row">
                        <div>
                            <img
                                className="-[85.95px] left-[150px] w-[150px] h-[30.06px]"
                                alt=""
                                src="/logo-aminsa.svg"
                            />
                            <div className=" mt-[15px] mb-3 left-[150px] text-xs leading-[140%] font-medium">
                                PT. CAHAYA AMERTA INTERNUSA
                            </div>
                            <div className="text-xs leading-[90%] font-medium inline-block w-[255px]">
                                <p >WTC Mall Matahari Serpong TA7-9,</p>
                                <p >Jalan Raya Serpong,</p>
                                <p >Kota Tangerang Selatan.</p>
                            </div>
                        </div>
                        <div>
                            <div className="mb-5 text-xl leading-[140%] font-medium">
                                Contact
                            </div>
                            <div className="leading-[140%] mb-3 font-medium">
                                0818 0567 2600
                            </div>
                            <div className="leading-[140%] font-medium">
                                info@cahayaamerta.com
                            </div>
                        </div>
                        <div>
                            <div className="mb-5 text-xl leading-[140%] font-medium">
                                Services
                            </div>
                            <div className="leading-[140%] mb-3 font-medium">
                                Branding
                            </div>
                            <div className="mb-3  leading-[140%] font-medium">
                                Product Photography
                            </div>
                            <div className="mb-3  leading-[140%] font-medium">
                                Social Media Management
                            </div>
                        </div>
                        <div>
                            <div className="mb-5 text-xl leading-[140%] font-medium">
                                Social Media
                            </div>
                            <div className="gap-2 grid grid-cols-3 justify-center items-center w-[140px] h-10">
                                <div className="rounded-11xl bg-white flex justify-center items-center p-2.5">
                                <img className="w-5 h-5" alt="" src="/vector.svg" />
                                </div>
                                <div className=" top-[0px] left-[50px] rounded-11xl bg-white flex flex-col p-2.5">
                                <img className="w-5 h-5" alt="" src="/vector1.svg" />
                                </div>
                                <div className=" top-[0px] left-[100px] rounded-11xl bg-white flex flex-col p-2.5">
                                <img
                                    className="w-5 h-[19.88px]"
                                    alt=""
                                    src="/vector2.svg"
                                />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className=" w-full flex flex-col mt-10 items-start justify-start">
                    <img
                    className="self-stretch max-w-full overflow-hidden h-0.5 shrink-0"
                    alt=""
                    src="/vector-4.svg"
                    />
                </div>
                <div className="flex text-center flex-row justify-center items-center mt-7 leading-[140%] font-medium">
                    © Copyright 2023 Aminsa Creative House | All Rights Reserved.
                </div>
            </div>
        </div>
    )
}

export default Footer_2