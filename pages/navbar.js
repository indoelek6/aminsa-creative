import Link from "next/link";
import { useCallback, useState, useEffect } from "react";
import { useRouter } from "next/router";

const Navbar = ({onFrameContainer11Click, onFrameContainer12Click, isNav, onFrameContainer13Click, onFrameContainer14Click, onFrameContainer15Click}) => {

    const [clientWindowHeight, setClientWindowHeight] = useState("");
    const [backgroundTransparacy, setBackgroundTransparacy] = useState(0);

    const router = useRouter()

    useEffect(() => {
        window.addEventListener("scroll", handleScroll);
        return () => window.removeEventListener("scroll", handleScroll);
    });

    const handleScroll = () => {
        setClientWindowHeight(window.scrollY);
    };

    useEffect(() => {
        let backgroundTransparacyVar = clientWindowHeight / 600;
    
        if (backgroundTransparacyVar < 1) {
          let paddingVar = 30 - backgroundTransparacyVar * 20;
          let boxShadowVar = backgroundTransparacyVar * 0.1;
          setBackgroundTransparacy(backgroundTransparacyVar);
        }
      }, [clientWindowHeight]);


    return (
    <div style={{background: `rgba(49, 43, 99, ${backgroundTransparacy})`}} className="fixed bg-darkslateblue-300 w-full flex flex-row py-[15px] px-[100px] box-border items-center justify-between text-xl">
        <img
            onClick={() => router.push('/page/LandingPages')}
            className="relative w-[148.63px] h-[41px]"
            alt=""
            src="/logo-aminsa1.svg"
        />
        <div className="flex flex-row items-start justify-center">
            <div
            className="h-[59px] flex flex-row p-[13px] box-border items-start justify-start cursor-pointer"
            onClick={() => {!isNav ? onFrameContainer11Click() : router.push("/page/LandingPages")}}
            >
            <div className="relative leading-[140%] text-white font-medium hover:text-yellow-400">About</div>
            </div>
            <div
            className="flex flex-row p-[13px] items-start justify-start cursor-pointer"
            onClick={() => {!isNav ? onFrameContainer12Click() :  router.push("/page/LandingPages")}}
            >
            <div className="relative leading-[140%] text-white font-medium hover:text-yellow-400">Process</div>
            </div>
            <div
            className="flex flex-row p-[13px] items-start justify-start cursor-pointer"
            onClick={() => router.push('/page/Portofolio')}
            >
            <div className="relative leading-[140%] text-white font-medium hover:text-yellow-400">Portfolio</div>
            </div>
            <div
            className="flex flex-row p-[13px] items-start justify-start cursor-pointer"
            onClick={() => router.push('/page/PriceList')}
            >
            <div className="relative leading-[140%] text-white font-medium hover:text-yellow-400">Pricelist</div>
            </div>
            <div
            className="flex flex-row p-[13px] items-start justify-start cursor-pointer"
            onClick={() => router.push('/page/TrackingProgress')}
            >
            <div className="relative leading-[140%] text-white font-medium hover:text-yellow-400">
                Tacking Progress
            </div>
            </div>
            <div className="rounded-3xl bg-white hover:bg-transparent hover:border-[2px] hover:text-white border-solid border-white flex flex-row p-[13px] items-start justify-start text-center ease-linear text-darkslateblue-100 ">
            <div className="relative leading-[140%] font-medium ">
                Inquire Now
            </div>
            </div>
        </div>
    </div>
    )
}

export default Navbar