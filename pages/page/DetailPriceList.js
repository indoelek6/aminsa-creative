import { useState } from "react"
import Footer from "../footer"
import Footer_2 from "../footer_2"
import Navbar from "../navbar"
import { useRouter } from "next/router"

const ListItem = [
    {   
        img: '/rectangle-13@2x.png',
        name: 'Starter Pack',
        price: 'Rp 1.000.000 /Month',
        taging: [
            {
                name: "Content Planning"
            },
            {
                name: "15 Feed"
            },
            {
                name: "Design & Copywriting"
            },
            {
                name: "Mirroring Stories"
            },
            {
                name: "Facebook Page Mirroring"
            },
            {
                name: "Revisi 1x / post"
            },
            {
                name: "Admin posting"
            },
            {
                name: "Hashtag research"
            },
            {
                name: "No comment & dm replies"
            },
            {
                name: "Free Content Hari Raya"
            },
        ],
        desc_name: 'For Your Daily Fruit Needs!',
        tagging: '#Social Media Management #Content Planning #Creative Manangement #Product Photography'
    },
    {
        img: '/rectangle-14@2x.png',
        name: 'Social Climber',
        price: "Rp 3.500.000 /Month",
        taging: [
            {
                name: "Content Planning"
            },
            {
                name: "15 Feed"
            },
            {
                name: "Design & Copywriting"
            },
            {
                name: "Mirroring Stories"
            },
            {
                name: "Facebook Page Mirroring"
            },
            {
                name: "Revisi 1x / post"
            },
            {
                name: "Admin posting"
            },
            {
                name: "Hashtag research"
            },
            {
                name: "No comment & dm replies"
            },
            {
                name: "Free Content Hari Raya"
            },
        ],
        desc_name: 'Kebersamaan Untuk Indonesia',
        tagging: '#Social Media Management #Content Creative #Content Planning #Content Management #Ads Production'
    },
    {
        img: '/rectangle-141@2x.png',
        name: 'Social Butterfly',
        price: "Rp 5.000.000 /Month",
        taging: [
            {
                name: "Content Planning"
            },
            {
                name: "15 Feed"
            },
            {
                name: "Design & Copywriting"
            },
            {
                name: "Mirroring Stories"
            },
            {
                name: "Facebook Page Mirroring"
            },
            {
                name: "Revisi 1x / post"
            },
            {
                name: "Admin posting"
            },
            {
                name: "Hashtag research"
            },
            {
                name: "No comment & dm replies"
            },
            {
                name: "Free Content Hari Raya"
            },
        ],
        desc_name: 'Indonesia Pasti Bisa Virtual Expo',
        tagging: '#Branding Identity #Virtual Expo #Social Media Management #Content Creative #Event Management'
    },
]

const DetailPriceList = () => {

    const router = useRouter()

    const [show,setShow] = useState("")

    return (
        <div className="relative bg-thistle w-full overflow-y-auto text-left text-xl font-heading-5-500">
            <Navbar isNav/>
            <div className="top-[0px] left-[0px] bg-darkslateblue-100 w-[1280px]" />
            <div className="bg-darkslateblue-100 w-full h-[100px] overflow-hidden text-center text-20xl text-white"/>
            <div className="w-full flex mt-20 flex-col items-center justify-start gap-[20px] text-center">
                <b className="relative leading-[140%] text-[39px] flex items-center justify-center w-[614px] h-[46px] shrink-0">
                Social Media Management
                </b>
                <div className="text-xl leading-[140%] inline-block w-[1080px] h-[98px] shrink-0">
                To equip your company with critical content creation and
                management skills, such as  creating effective social media posts
                and how to create a strong brand to help you build a social media
                presence. Our company will help you to establish an ongoing
                process to manage your content.
                </div>
            </div>
            <div className="gap-[48px] grid grid-cols-3 px-16 mt-[68px] left-[140px]">
            {ListItem.map((x) => {
                return (
                    <div style={{height: show  !== x ? "500px" : null, minHeight: "200px"}} className="rounded-t-281xl rounded-b-xl rounded-t-[300px] bg-white w-[340px] flex flex-col p-[25px] box-border items-start justify-start gap-[10px]">
                        <div className="flex flex-col pt-0 px-0 pb-2.5 items-start justify-start">
                            <img
                            className=" rounded-t-281xl rounded-b-none w-[290px] h-[250px] object-cover"
                            alt=""
                            src={x.img}
                            />
                        </div>
                        <div className="self-stretch flex flex-row items-center justify-start text-5xl">
                            <b className="flex-1 ">{x.name}</b>
                            <img
                            className=" w-[26px] h-[26px]"
                            alt=""
                            src="/group-1.svg"
                            onClick={() => {
                                if (x == show) {
                                    setShow("")
                                }
                                else {
                                    setShow(x)
                                }
                            }}
                            />
                        </div>
                        <b className="leading-[140%] text-orange">
                            {x.price}
                            <div className="w-full h-[0.8px] mt-3 mb-2 bg-darkslateblue-100"/>
                        </b>
                        {show == x &&
                        <div>
                            <div className="text-[13px] mb-3">
                                {x.taging.map(e => {
                                    return (
                                        <div className="mb-1">
                                           - {e.name}
                                        </div>
                                    )
                                })}
                            </div>
                        </div>
                        }
                        <div className="text-white justify-center items-center flex flex-row self-center">
                            <div className="rounded-3xl border-[2px] border-solid hover:border-darkslateblue-100 bg-darkslateblue-100 hover:bg-transparent py-2.5 px-5 text-white hover:text-darkslateblue-100" onClick={() => router.push("/page/OrderDetails")}>
                                <div className="leading-[140%] font-medium">
                                    Book Now
                                </div>
                            </div>
                        </div>
                        </div>
                )
            })}
            </div>
            <div className="mt-28"/>
            <div className="w-full flex mt-20 flex-col items-center justify-start gap-[20px] text-center">
                <b className="leading-[140%] mb-8 text-[39px] flex items-center justify-center w-[614px] h-[46px] shrink-0">
                You can’t find package<br/>that fits into you?
                </b>
                <div className="text-xl leading-[140%] inline-block w-[1080px] shrink-0">
                Click button below to customize your package
                </div>
                <div className="rounded-3xl border-[2px] border-solid hover:border-darkslateblue-100 bg-darkslateblue-100 hover:bg-transparent flex flex-row py-4 px-[25px] items-center justify-center text-white hover:text-darkslateblue-100">
                  <div className="leading-[140%] font-medium">
                    Customize your package
                  </div>
                </div>
            </div>
            <div className="mt-16"/>
            <Footer_2/>
        </div>
    )
}

export default DetailPriceList