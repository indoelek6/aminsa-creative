import Footer from "../footer"
import Footer_2 from "../footer_2"
import Navbar from "../navbar"
import { useRouter } from "next/router"

const ListItem = [
    {   
        img: '/rectangle-38@2x.png',
        name: 'Frutas Buah',
        desc_name: 'For Your Daily Fruit Needs!',
        tagging: '#Social Media Management #Content Planning #Creative Manangement #Product Photography'
    },
    {
        img: '/rectangle-384@2x.png',
        name: 'Kongres Ilumni FH UNPAR',
        desc_name: 'Kebersamaan Untuk Indonesia',
        tagging: '#Social Media Management #Content Creative #Content Planning #Content Management #Ads Production'
    },
    {
        img: '/rectangle-385@2x.png',
        name: 'Nasional Is Me!',
        desc_name: 'Indonesia Pasti Bisa Virtual Expo',
        tagging: '#Branding Identity #Virtual Expo #Social Media Management #Content Creative #Event Management'
    },
    {   
        img: '/rectangle-381@2x.png',
        name: 'Waltie’s Rooftop Cafe',
        desc_name: 'Delicious Drinks & Foods!',
        tagging: '#Social Media Management #Content Planning #Creative Manangement #Product Photography'
    },
    {
        img: '/rectangle-382@2x.png',
        name: 'Demokrasi',
        desc_name: 'Debat Modern Kreatif Generasi',
        tagging: '#Event Management #Social Media Management #Content Planning #Creative Manangement'
    },
    {
        img: '/rectangle-383@2x.png',
        name: 'Baku Dapa G&G',
        desc_name: '100% Halal Manadonese Food',
        tagging: '#Social Media Management #Content Planning #Creative Manangement #Product Photography'
    },
]

const Portfolio = () => {

    const router = useRouter()

    const hanleViewPdf = () => {
        router.push("/page/PDFView")
    }

    return (
        <div className="relative bg-thistle w-full h-[2473px] overflow-y-auto text-left text-xl font-heading-5-500">
            <Navbar isNav/>

            <div className="top-[0px] left-[0px] bg-darkslateblue-100 w-[1280px]" />
            <div className="bg-darkslateblue-100 w-full h-[300px] overflow-hidden text-center text-20xl text-white">
                <img
                className="top-[0px] left-[calc(50%_-_640px)] w-[1280px] h-[293px]"
                alt=""
                src="/img-overlay.svg"
                data-scroll-to="imgOverlay"
                />
                <b className="absolute top-[160px] left-[calc(50%_-_66px)] leading-[140%]">
                Works
                </b>
            </div>
            <div className="mt-[70px] w-full flex flex-col py-0 px-[30px] items-center text-center justify-between gap-[20px] text-5xl">
                <div className="flex flex-row text-center items-start justify-between gap-[50px]">
                <b className="relative leading-[140%]">All</b>
                <div className="relative leading-[140%] font-medium">
                    Social Media Management
                </div>
                <div className="relative leading-[140%] font-medium">
                    Graphic Design
                </div>
                <div className="relative leading-[140%] font-medium">
                    Brand identity
                </div>
                </div>
                <div className="flex flex-row items-start justify-start gap-[50px]">
                <div className="relative leading-[140%] font-medium">
                    Product Photography
                </div>
                <div className="relative leading-[140%] font-medium">
                    Ads Production
                </div>
                <div className="relative leading-[140%] font-medium">
                    Event Management
                </div>
                </div>
            </div>
            <div className="gap-[48px] grid grid-cols-3 px-16 mt-[98px] left-[140px]">
            {ListItem.map((x) => {
                return (
                    <div className="w-[340px] h-[680px] rounded-b-xl rounded-t-[300px] bg-white overflow-hidden flex flex-col  items-center justify-center gap-[25px]">
                        <img
                            className="rounded-t-281xl rounded-b-none w-[297px] h-[285px] object-cover"
                            alt=""
                            src={x.img}
                        />
                        <div className="flex flex-col items-start justify-start gap-[10px]">
                        <b className="self-stretch">{x.name}</b>
                        <div className="text-base leading-[140%] font-medium flex items-center w-[290px]">
                            {x.desc_name}
                        </div>
                        </div>
                        <img className="w-[297px] h-px" alt="" src="/vector-1.svg" />
                        <div className="text-sm leading-[120%] text-orange flex items-center w-[290px]">
                            {x.tagging}
                        </div>
                        <div className="rounded-3xl cursor-pointer border-[2px] border-solid hover:border-darkslateblue-100 bg-darkslateblue-100 hover:bg-transparent flex flex-row py-2.5 px-[21px] items-start justify-start text-center text-smi text-white hover:text-darkslateblue-100"  onClick={() => hanleViewPdf()}>
                            <div className="leading-[140%] font-medium">See More</div>
                        </div>
                    </div>
                )
            })}
            </div>
            <div className="mt-16"/>
            <Footer_2 contact/>
        </div>
    )
}

export default Portfolio