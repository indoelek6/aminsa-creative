import { useState } from "react"
import Footer from "../footer"
import Footer_2 from "../footer_2"
import Navbar from "../navbar"
import { useRouter } from "next/router"

const ListItem = [
    {   
        img: '/rectangle-38@2x.png',
        name: 'Frutas Buah',
        desc_name: 'For Your Daily Fruit Needs!',
        tagging: '#Social Media Management #Content Planning #Creative Manangement #Product Photography'
    },
    {
        img: '/rectangle-384@2x.png',
        name: 'Kongres Ilumni FH UNPAR',
        desc_name: 'Kebersamaan Untuk Indonesia',
        tagging: '#Social Media Management #Content Creative #Content Planning #Content Management #Ads Production'
    },
]

const TrackingProgress = () => {

    const router = useRouter()

    const [show,setShow] = useState("")

    return (
        <div className="relative bg-thistle w-full overflow-y-auto text-left text-xl font-heading-5-500">
            <Navbar isNav/>
            <div className="top-[0px] left-[0px] bg-darkslateblue-100 w-[1280px]" />
            <div className="bg-darkslateblue-100 w-full h-[100px] overflow-hidden text-center text-20xl text-white"/>
            <div className="w-full flex mt-20 flex-col items-center justify-start gap-[20px] text-center">
                <b className="relative leading-[140%] text-[39px] flex items-center justify-center w-full h-[46px] shrink-0">
                Track Your Post-Production Progress
                </b>
                <div className=" leading-[140%] text-[16px] inline-block w-[1080px] h-[98px] shrink-0">
                Now you can track the progress of your documentation<br/>results by only write your Order ID!
                </div>
            </div>
            <div className="flex flex-row mt-5  items-center justify-center text-center text-base text-darkgray">
                <div className="w-full h-[43px] items-center justify-center text-center flex flex-row">
                    <div className="left-[0px] rounded-[20px] content-center self-center bg-white w-[460px] h-[43px]">
                        <input className=" mt-[6px] mr-0 placeholder:leading-[140%] outline-none text-sm w-[420px] h-7" placeholder="12345678"/>
                    </div>
                </div>
            </div>
            <div className="w-full items-center justify-center text-center flex flex-row">
                <div onClick={() => router.push('/page/DetailTracking')} className="rounded-3xl w-[120px] mt-9 bg-darkslateblue-100 hover:bg-transparent hover:border-[2px] hover:text-darkslateblue-100 border-solid hover:border-darkslateblue-100 flex flex-row py-4 px-[25px] items-center justify-center self-center  text-center text-xl text-white">
                    <div className=" text-[15px]">
                    Check Now
                    </div>
                </div>
            </div>
            <div className="mt-28"/>
            <Footer_2/>
        </div>
    )
}

export default TrackingProgress