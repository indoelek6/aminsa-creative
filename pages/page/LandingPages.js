import { useCallback } from "react";
import Footer from "../footer";
import Navbar from "../navbar";
import { useRouter } from "next/router";

let Process = [
  {
    name: "Brief",
    desc: "We first want to know you and your business story. What are you trying to achieve, what are the expected deliverables, what's the time frame of the project, and where do you see us fitting in?."
  },
  {
    name: "Research",
    desc: "After meeting you we then do our homework. This includes industry and company research; what's trending in the market, what has been done in the past and should be done or avoided in the future, what are the best practices and how we can help you meet these."
  },
  {
    name: "Proposal",
    desc: "This includes our job plan & tactics, timelines, and budgets how we plan to help you achieve your marketing goals, and what you can expect in the process."
  },
  {
    name: "Research",
    desc: "Once you've reviewed and agreed with our proposal, we then proceed with the implementation of tasks. Ready to kick off your project as per the agreed timeline. We’re proud to have worked with incredible brands across multiple industries in numerous countries. Check out our portfolio for more details of our work."
  }
]

const LandingPage = () => {

    const router = useRouter()

  const onFrameContainer7Click = useCallback(() => {
    const anchor = document.querySelector("[data-scroll-to='whatWeDo']");
    if (anchor) {
      anchor.scrollIntoView({ block: "start", behavior: "smooth" });
    }
  }, []);

  const onFrameContainer8Click = useCallback(() => {
    // Please sync "Works" to the project
  }, []);

  const onFrameContainer11Click = useCallback(() => {
    const anchor = document.querySelector("[data-scroll-to='whatWeDo']");
    if (anchor) {
      anchor.scrollIntoView({ block: "center", behavior: "smooth", });
    }
  }, []);

  const onFrameContainer12Click = useCallback(() => {
    const anchor = document.querySelector(
      "[data-scroll-to='processContainer']"
    );
    if (anchor) {
      anchor.scrollIntoView({ block: "center", behavior: "smooth" });
    }
  }, []);

  const onFrameContainer13Click = useCallback(() => {
    // Please sync "Works" to the project
  }, []);

  const onFrameContainer14Click = useCallback(() => {
    // Please sync "Pricelist" to the project
  }, []);

  const onFrameContainer15Click = useCallback(() => {
    // Please sync "Tracking Progress" to the project
  }, []);

  return (
    <div>
      <div className="relative bg-white w-full h-[3722px] overflow-y-auto text-left text-20xl text-white font-heading-5-500">
        <div
          className="absolute top-[720px] left-[1px] w-full h-[720px] text-center text-darkslateblue-100"
          data-scroll-to="whatWeDo"
        >
          <div className="absolute top-[75px] left-[100px] w-0 h-0 text-left">
            <div className="absolute top-[464px] left-[0px] w-[1080px] hidden flex-row items-start justify-start gap-[20px]">
              <div className="relative w-[347px] h-[148px]">
                <div className="absolute top-[0px] left-[0px] bg-orange w-[347px] h-[148px]" />
                <b className="absolute top-[0px] left-[calc(50%_-_173.5px)] leading-[140%] flex items-center w-[347px] h-[29px]">
                  Branding
                </b>
              </div>
              <div className="relative w-[347px] h-[148px]">
                <div className="absolute top-[0px] left-[calc(50%_-_173.5px)] bg-orange w-[347px] h-[148px]" />
                <b className="absolute top-[calc(50%_-_34px)] left-[calc(50%_-_173.5px)] leading-[100%] flex items-center w-[347px] h-[67px]">
                  Social Media Management
                </b>
              </div>
              <div className="relative w-[346px] h-[148px]">
                <div className="absolute top-[0px] left-[calc(50%_-_173px)] bg-orange w-[346px] h-[148px]" />
                <b className="absolute top-[118.53px] left-[calc(50%_-_173px)] leading-[140%] flex items-center w-[308.11px] h-[29.47px]">
                  Marketing
                </b>
              </div>
            </div>
            <div className="absolute top-[422px] left-[0px] w-[1080px] hidden flex-row items-end justify-start gap-[20px]">
              <div className="relative w-[347px] h-[148px]">
                <div className="absolute top-[0px] left-[0px] bg-orange w-[347px] h-[148px]" />
                <b className="absolute top-[0px] left-[calc(50%_-_173.5px)] leading-[100%] flex items-center w-[347px] h-[67px]">
                  Product Photography
                </b>
              </div>
              <div className="relative w-[347px] h-[148px]">
                <div className="absolute top-[0px] left-[calc(50%_-_173.5px)] bg-orange w-[347px] h-[148px]" />
                <div className="absolute top-[0px] left-[calc(50%_-_173.5px)] bg-orange w-[347px] h-[148px]" />
                <div className="absolute top-[0px] left-[calc(50%_-_173.5px)] bg-orange w-[347px] h-[148px]" />
                <b className="absolute top-[35px] left-[calc(50%_-_173.5px)] leading-[100%] flex items-center w-[347px]">
                  Event Management
                </b>
              </div>
              <div className="relative w-[346px] h-[148px]">
                <div className="absolute top-[0px] left-[calc(50%_-_173px)] bg-orange w-[346px] h-[148px]" />
                <b className="absolute top-[79px] left-[calc(50%_-_173px)] leading-[100%] flex items-center w-[346px] h-[67px]">
                  Public Relation Strategy
                </b>
              </div>
            </div>
          </div>
          <div className="absolute top-[75px] w-full h-[164px]">
            <b className="absolute top-[0px] leading-[140%] flex items-center justify-center w-full h-[46px]">
              What We Do?
            </b>
            <div className="absolute top-[66px] text-xl leading-[140%] flex items-center justify-center w-full h-[98px]">
              Breaking through the digital era, at Aminsa Creative House, we are
              dedicated to providing you with the very<br/>highest levels of service
              in all that we do. This is our service commitment. It infuses every
              aspect of our<br/>business; from our ethos, to the training of our
              people and the development of our relationship with you.
            </div>
          </div>
          <div className="absolute top-[299px] left-[calc(50%_-_540px)]  flex flex-row justify-center text-center items-center w-[347px] h-[148px]  hover:justify-center hover:text-center bg-orange hover:bg-darkslateblue-100 hover:text-orange hover:items-center">
              <b className="relative leading-[100%]">
                Branding
              </b>
          </div>
          <div className="absolute top-[299px] left-[calc(50%_-_173px)] flex flex-row justify-center text-center items-center w-[346px] h-[148px] bg-orange hover:bg-darkslateblue-100 hover:text-orange">
              <b className="relative leading-[100%]">
                Social Media<br/>Management
              </b>
          </div>
          
          <div className="absolute top-[299px] right-[180px] flex flex-row items-center justify-center w-[347px] h-[148px] hover:justify-center text-right hover:text-center bg-orange hover:bg-darkslateblue-100 hover:text-orange hover:items-center">
              <b className="relative leading-[100%]">
                Marketing
              </b>
          </div>
          <div className="absolute top-[467px] left-[calc(50%_-_540px)] w-[347px] h-[148px] flex flex-row justify-center text-center items-center bg-orange hover:bg-darkslateblue-100 hover:text-orange">
              <b className="relative leading-[100%]">
                  Product<br/>Photography
              </b>
          </div>
          <div className="absolute top-[467px] left-[calc(50%_-_173px)] w-[347px] h-[148px] flex flex-row justify-center text-center items-center bg-orange hover:bg-darkslateblue-100 hover:text-orange">
              <b className="relative leading-[100%]">
                  Event<br/>Management
              </b>
          </div>
          <div className="absolute top-[467px] right-[180px] w-[347px] h-[148px] flex flex-row justify-center text-center items-center bg-orange hover:bg-darkslateblue-100 hover:text-orange">
              <b className="relative leading-[100%]">
                  Ads<br/>Production
              </b>
          </div>
        </div>
        <div
          className="absolute top-[1440px] bg-thistle w-full h-[737px] text-darkslateblue-100"
          data-scroll-to="processContainer"
        >
          <div className="absolute top-[75px] w-full h-[150px]">
            <b className="absolute top-[0px] leading-[140%] items-center flex justify-center w-full h-[46px]">
              Our Process
            </b>
            <div className="absolute top-[66px] flex items-center justify-center text-xl leading-[140%] text-center w-full">
              We love connecting with businesses from various backgrounds and
              industries. It’s important for us to know<br/>your story, what drives
              you, and what your goals are. Our mission is to help you grow in the
              digital space<br/>and be proud of the image we help you represent
              online.
            </div>
          </div>
          <div className="absolute top-[195px] gap-0 grid grid-cols-2 text-base">
            {Process.map((x, i) => {
              return (
                <div className="xl:p-28 w-auto h-0">
                  <b className="inline-block mb-5">{i + 1}. {x.name}</b>
                  <div className="inline-block text-base">{x.desc}</div>
                </div>
              )
            })}
          </div>
        </div>
        <div className="absolute top-[2177px] left-[0px] w-[1280px] h-[720px] text-darkslateblue-100">
          <div className="absolute top-[0px] left-[calc(50%_-_640px)] bg-white w-[1280px] h-[720px]" />
          <div className="absolute top-[401px] left-[180px] w-[281px] h-[244px]">
            <div className="absolute top-[0px] left-[89px] w-48 h-[141px]">
              <img
                className="absolute top-[71.97px] left-[123.11px] w-[68.89px] h-[69.03px] object-cover"
                alt=""
                src="/layer-1@2x.png"
              />
              <img
                className="absolute top-[0px] left-[0px] w-[102.6px] h-[102.81px] object-cover"
                alt=""
                src="/layer-3@2x.png"
              />
            </div>
            <div className="absolute top-[141px] left-[0px] w-48 h-[103px]">
              <img
                className="absolute top-[0px] left-[89.4px] w-[102.6px] h-[103px] object-cover"
                alt=""
                src="/layer-4@2x.png"
              />
              <img
                className="absolute top-[17.66px] left-[0px] w-[68.89px] h-[69.16px] object-cover"
                alt=""
                src="/layer-2@2x.png"
              />
            </div>
          </div>
          <div className="absolute top-[75px] w-full h-[177px]">
            <b className="absolute top-[0px] left-[100px] leading-[140%] flex items-center w-[251px] h-[46px]">
              Latest Work
            </b>
            <div className="absolute top-[66px] left-[100px] text-xl leading-[140%] flex items-center w-[431px] h-[111px]">
              We're super proud to have worked with some amazing brands in
              Indonesia, and internationally. Check out our portfolio for more
              details of our work.
            </div>
          </div>
          <div onClick={() => router.push("/page/Portofolio")} className="absolute top-[292px] left-[100px] rounded-3xl border-[2px] bg-darkslateblue-100 border-solid hover:bg-transparent hover:border-darkslateblue-100 flex flex-row py-4 px-[25px] items-start justify-start text-center text-xl text-white hover:text-darkslateblue-100">
            <div className="relative leading-[140%] font-medium">
              MORE PORTFOLIO
            </div>
          </div>
          <img
            className="absolute top-[149px] left-[683px] w-[597px] h-[422px] object-cover"
            alt=""
            src="/frutas-buah--mockup-1-2@2x.png"
          />
        </div>
        <div className="absolute top-[2897px] left-[1px] w-full h-[170px]">
          <div className="absolute top-[0px] bg-thistle w-full h-[170px]" />
          <div className="absolute top-[calc(50%_-_75px)] left-[calc(50%_-_554.5px)] w-[1110px] h-[150px]">
            <div className="absolute top-[0px] left-[15px] w-[1080px] h-[150px] overflow-hidden">
              <div className="absolute top-[0px] left-[15px] overflow-hidden flex flex-row items-start justify-start gap-[30px]">
                <img
                  className="relative rounded-[50%] w-[150px] h-[150px] object-cover"
                  alt=""
                  src="/ellipse-1@2x.png"
                />
                <img
                  className="relative rounded-[50%] w-[150px] h-[150px] object-cover"
                  alt=""
                  src="/ellipse-2@2x.png"
                />
                <img
                  className="relative rounded-[50%] w-[150px] h-[150px] object-cover"
                  alt=""
                  src="/ellipse-3@2x.png"
                />
                <img
                  className="relative rounded-[50%] w-[150px] h-[150px] object-cover"
                  alt=""
                  src="/ellipse-4@2x.png"
                />
                <img
                  className="relative rounded-[50%] w-[150px] h-[150px] object-cover"
                  alt=""
                  src="/ellipse-5@2x.png"
                />
                <img
                  className="relative rounded-[50%] w-[150px] h-[150px] object-cover"
                  alt=""
                  src="/ellipse-6@2x.png"
                />
                <img
                  className="relative rounded-[50%] w-[150px] h-[150px] object-cover"
                  alt=""
                  src="/ellipse-7@2x.png"
                />
              </div>
              <div className="absolute top-[0px] left-[1275px] flex flex-row items-start justify-start gap-[30px]">
                <img
                  className="relative rounded-[50%] w-[150px] h-[150px] object-cover"
                  alt=""
                  src="/ellipse-7@2x.png"
                />
                <img
                  className="relative rounded-[50%] w-[150px] h-[150px] object-cover"
                  alt=""
                  src="/ellipse-7@2x.png"
                />
                <img
                  className="relative rounded-[50%] w-[150px] h-[150px] object-cover"
                  alt=""
                  src="/ellipse-7@2x.png"
                />
                <img
                  className="relative rounded-[50%] w-[150px] h-[150px] object-cover"
                  alt=""
                  src="/ellipse-7@2x.png"
                />
                <img
                  className="relative rounded-[50%] w-[150px] h-[150px] object-cover"
                  alt=""
                  src="/ellipse-7@2x.png"
                />
              </div>
              <div className="absolute top-[0px] left-[2175px] flex flex-row items-start justify-start gap-[30px]">
                <img
                  className="relative rounded-[50%] w-[150px] h-[150px] object-cover"
                  alt=""
                  src="/ellipse-7@2x.png"
                />
                <img
                  className="relative rounded-[50%] w-[150px] h-[150px] object-cover"
                  alt=""
                  src="/ellipse-7@2x.png"
                />
                <img
                  className="relative rounded-[50%] w-[150px] h-[150px] object-cover"
                  alt=""
                  src="/ellipse-7@2x.png"
                />
                <img
                  className="relative rounded-[50%] w-[150px] h-[150px] object-cover"
                  alt=""
                  src="/ellipse-7@2x.png"
                />
                <img
                  className="relative rounded-[50%] w-[150px] h-[150px] object-cover"
                  alt=""
                  src="/ellipse-7@2x.png"
                />
                <img
                  className="relative rounded-[50%] w-[150px] h-[150px] object-cover"
                  alt=""
                  src="/ellipse-7@2x.png"
                />
              </div>
            </div>
            <img
              className="absolute top-[60px] left-[0px] w-[30px] h-[30px] overflow-hidden opacity-[0]"
              alt=""
              src="/button-left.svg"
            />
            <img
              className="absolute top-[calc(50%_-_15px)] left-[1080px] w-[30px] h-[30px] overflow-hidden"
              alt=""
              src="/button-right.svg"
            />
          </div>
        </div>
        <Footer isSection/>
        <Navbar
          onFrameContainer11Click={() => onFrameContainer11Click()}
          onFrameContainer12Click={() => onFrameContainer12Click()}
          handleClick={(e, link) => alert(link)}
        />
      </div>
    </div>
  );
};

export default LandingPage;
