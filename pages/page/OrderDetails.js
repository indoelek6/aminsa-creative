import Image from "next/image"
import { useState } from "react"
import Footer_2 from "../footer_2"
import Navbar from "../navbar"
import IC_BANK from "../../public/IC_BANK.png"
import IC_BCA from "../../public/IC_BCA.png"
import IC_VISA from "../../public/IC_VISA.png"
import IC_WALLET from "../../public/IC_WALLET.png"
import IC_BCA_2 from "../../public/IC_BCA_2.svg"
import IC_DROP_DOWN from "../../public/IC_DROP_DOWN.svg"
import { useRouter } from "next/router"


const PaymentList = [
    {
        icon: IC_VISA,
        name: "Credit/Debit Cards"
    },
    {
        icon: IC_BCA,
        name: "BCA Virtual Account"
    },
    {
        icon: IC_BANK,
        name: "ATM/Bank Transfer"
    },
    {
        icon: IC_WALLET,
        name: "Bank Center"
    },
]

const GuidePayment = [
    {
        name: "Credit/Debit Cards"
    },
    {
        name: "BCA Virtual Account"
    },
    {
        name: "ATM/Bank Transfer"
    },
]

const OrderDetails = () => {

    const router = useRouter()

    const [isPayment,setIsPayment] = useState(false)
    const [selectPayment,setSelectPayment] = useState(false)
    const [modalPayment, setModalPayment] = useState({
        state: 0
    })
    const [name, setName] = useState("")
    const [email, setEmail] = useState("")
    const [phone, setPhone] = useState("")
    const [generateCode, setGenerateCode] = useState("")

    return (
        <div className="bg-thistle w-full overflow-y-auto font-heading-5-500">
            <Navbar isNav/>
            <div className="top-[0px] left-[0px] bg-darkslateblue-100 w-[1280px]" />
            <div className="bg-darkslateblue-100 w-full h-[100px] overflow-hidden text-center text-20xl text-white"/>
            <div className="mt-24"/>
            <div className="flex flex-row mt-5 justify-between px-24">
                {isPayment == false ?
                <div>
                    <div className="w-[460px] h-[257px] mt-1 text-black">
                        <b className="text-9xl leading-[100%] text-darkslateblue">
                        Customer Details
                        </b>
                        <div className="mt-10 w-[460px] h-[61px]">
                        <div className="leading-[100%] mb-2 text-[16px] font-medium text-darkslateblue">
                            Full Name
                        </div>
                        <div className=" h-[65.57%] w-full right-[0%] mb-3 bottom-[0%]  rounded-11xl bg-white box-border border-[1px] border-solid border-gray-400">
                            <input type="text" className="h-[62.57%] w-[435px] mt-[5px] ml-2  outline-none border-transparent" onChange={(e) => setName(e.target.value)}/>
                        </div>
                        <div className="leading-[100%] mb-2 text-[16px] font-medium text-darkslateblue">
                            Email
                        </div>
                        <div className=" h-[65.57%] w-full right-[0%] mb-3 bottom-[0%]  rounded-11xl bg-white box-border border-[1px] border-solid border-gray-400">
                            <input className="h-[62.57%] w-[435px] mt-[5px] outline-none ml-2" onChange={(e) => setEmail(e.target.value)}/>
                        </div>
                        <div className="leading-[100%] mb-2 text-[16px] font-medium text-darkslateblue">
                            Phone Number
                        </div>
                        <div className=" h-[65.57%] w-full right-[0%] mb-3 bottom-[0%]  rounded-11xl bg-white box-border border-[1px] border-solid border-gray-400">
                            <input className="h-[62.57%] w-[435px] mt-[5px] outline-none ml-2" onChange={(e) => setPhone(e.target.value)}/>
                        </div>
                        </div>
                    </div>
                    <div className="w-[460px] h-[257px] mt-28 text-black">
                        <b className="text-9xl leading-[100%] text-darkslateblue">
                        Brand Details
                        <div className="leading-[100%] mt-2 text-[14px] font-medium">Help us to know what kind of your brand</div>
                        </b>
                        <div className="mt-10 w-[460px] h-[61px]">
                        <div className="leading-[100%] mb-2 text-[16px] font-medium text-darkslateblue">
                            Brand Name
                        </div>
                        <div className=" h-[65.57%] w-full right-[0%] mb-3 bottom-[0%]  rounded-11xl bg-white box-border border-[1px] border-solid border-gray-400">
                            <input className="h-[62.57%] w-[435px] outline-none mt-[5px] ml-2"/>
                        </div>
                        <div className="leading-[100%] mb-2 text-[16px] font-medium text-darkslateblue">
                            Category
                        </div>
                        <div className=" h-[65.57%] w-full right-[0%] mb-3 bottom-[0%]  rounded-11xl bg-white box-border border-[1px] border-solid border-gray-400">
                            <input className="h-[62.57%] w-[435px] outline-none mt-[5px] ml-2"/>
                        </div>
                        <div className="leading-[100%] mb-2 text-[16px] font-medium text-darkslateblue">
                            Instagram
                        </div>
                        <div className=" h-[65.57%] w-full right-[0%] mb-3 bottom-[0%]  rounded-11xl bg-white box-border border-[1px] border-solid border-gray-400">
                            <input className="h-[62.57%] w-[435px] outline-none mt-[5px] ml-2"/>
                        </div>
                        <div className="leading-[100%] mb-2 text-[16px] font-medium text-darkslateblue">
                            About Your Brand
                        </div>
                        <div className=" h-[275.57%] w-full right-[0%] mb-3 bottom-[0%]  rounded-11xl bg-white box-border border-[1px] border-solid border-gray-400">
                            <input className="h-[62.57%] rounded-lg outline-none w-[435px] mt-[8px] ml-2"/>
                        </div>
                        </div>
                    </div>
                </div>
                :
                <div className="mt-1">
                    <b className="text-9xl leading-[100%] text-darkslateblue">
                        Payment Details
                        <div className="leading-[100%] mt-3 text-[14px] font-medium">We accept multiple payment methods:</div>
                    </b>
                    <div className="mt-10"/>
                    {PaymentList.map(x => {
                        return (
                            <div onClick={() => {
                                if (x == selectPayment) {
                                    setSelectPayment("")
                                }
                                else {
                                    setSelectPayment(x)
                                }
                            }} style={{borderColor: selectPayment !== x ? '#A0A0A0' : '#312B63', borderWidth: selectPayment == x ? "2px" : "1px"}} className="rounded-xl mb-5 box-border w-[460px] h-[60px] flex flex-row py-2.5 px-[18px] items-center justify-between border-[1px] border-solid border-darkgray">
                                <div className="leading-[100%] font-medium">
                                    {x.name}
                                </div>
                                <Image
                                // className="relative w-[47px] h-[15.67px] object-cover"
                                alt=""
                                src={x.icon}
                                />
                            </div>
                        )
                    })}
                    <div className="flex flex-row items-center mt-14 justify-between w-[460px] h-[61px] text-mini text-black">
                        <div className="h-[98.57%] w-[74.78%]">
                            <div className="text-base leading-[100%] ,t font-medium text-darkslateblue-100">
                            Redeem Code
                            </div>
                            <div className="h-[65.57%] mt-3 rounded-11xl bg-white box-border border-[1px] border-solid border-gray-200">
                                <input value={generateCode} className="h-[62.62%] w-[87.39%] mt-1.5 ml-4 bottom-[11.48%] left-[4.13%] bg-white" />
                            </div>
                        </div>
                        <div onClick={() => setGenerateCode("AS12343")} className="rounded-3xl mt-8 hover:border-[2px] border-solid hover:border-darkslateblue-100 bg-darkslateblue-100 hover:bg-transparent flex flex-row py-2.5 px-[15px] box-border items-start justify-start text-center text-white hover:text-darkslateblue-100">
                            <div className="leading-[140%] font-medium">Get Code</div>
                        </div>
                    </div>
                </div>
                }
                <div className="w-[520px] h-[768px] text-center text-sm text-darkslateblue-100">
                    <img
                    className="w-[574px] h-[822px]"
                    alt=""
                    src="/subtract.svg"
                    />
                    <div className="mt-[-780px] ml-[70px] ">
                        <div className="w-[277.33px] h-[104px] text-[34px]">
                            <div className="ml-[75px]">
                                <b className="leading-[100%] inline-block text-[35px] w-[277.33px] h-[68px]">
                                    <p className="m-0">Social Climber</p>
                                    <p className="m-0">Package</p>
                                </b>
                                <div className="text-base leading-[100%] font-medium text-orange inline-block w-[277.33px] h-4">
                                    Order ID : 072023001
                                </div>
                            </div>
                            <div className="absolute my-5 flex flex-row w-[420px] border-[1px] border-dashed border-darkslateblue-100"/>
                            <div className="w-[260px] h-[97px] text-left mt-8">
                                <b className="text-[20px] leading-[100%] inline-block w-[197.17px] h-5 mb-5">
                                    Customer Details
                                </b>
                                <div className="leading-[100%] text-[14px] w-[260px] h-3.5 mb-2">
                                    Name : {name}
                                </div>
                                <div className="leading-[100%] text-[14px] w-[276.17px] h-3.5 mb-2">
                                    Email : {email}
                                </div>
                                <div className="leading-[%] text-[14px] w-[172.25px] h-3.5 mb-2">
                                    Phone : {phone}
                                </div>
                            </div>
                            <div className="absolute my-5 mt-[40px] flex flex-row w-[420px] border-[1px] border-dashed border-darkslateblue-100"/>
                    </div>
                    </div>
                    <div className="w-[334px] mt-[200px] h-[65.85px] text-xl">
                        <b className="leading-[100%] ml-[220px] inline-block w-[139px] h-[18.46px]">
                            Order Details
                        </b>
                        <div className="flex flex-row ml-[135px] mt-3 items-start justify-start gap-[5px] text-xs">
                            <div className="rounded-xl flex flex-row py-2.5 px-1 items-start justify-start border-[2px] border-solid border-darkslateblue">
                                <div className="leading-[100%] min-w-[80px] font-medium">
                                    30 IG Feeds
                                </div>
                            </div>
                            <div className="rounded-xl flex flex-row py-2.5 px-1 items-start justify-start border-[2px] border-solid border-darkslateblue">
                                <div className="leading-[100%] min-w-[80px] font-medium">
                                    15 IG Stories
                                </div>
                            </div>
                            <div className="rounded-xl flex flex-row py-2.5 px-5 items-start justify-start border-[2px] border-solid border-darkslateblue">
                                <div className="leading-[100%] min-w-[60px] font-medium">
                                    5 IG Reels
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="w-[420px] ml-[70px] h-16 text-left mt-[35px]">
                        <div className="flex flex-row justify-between items-center">
                            <div className="leading-[100%] font-medium">
                                Booking Date:
                            </div>
                            <b className="leading-[100%] text-right">
                                15 Juni 2023
                            </b>
                        </div>
                        <div className="flex flex-row justify-between mt-[20px] items-center">
                            <div className="flex flex-row">
                                <div className="w-[25px] h-[25px]">
                                    <div className="box-border w-[25px] h-[25px] border-[1px] border-dashed border-darkslateblue-100 justify-center flex flex-row items-center">
                                        <div className="leading-[100%] font-medium">
                                        1
                                        </div>
                                    </div>
                                </div>
                                <div className="text-xs leading-[100%] font-medium ml-[20px]">
                                    <p className="m-0">Social Climber</p>
                                    <p className="m-0">Package</p>
                                </div>
                            </div>
                            <b className="leading-[100%] text-orange text-right">
                                Rp 3.500.000
                            </b>
                        </div>
                        {isPayment &&
                        <div className="flex flex-row justify-between mt-[20px] items-center">
                            <div className="flex flex-row">
                                <div className="w-[25px] h-[25px]">
                                    <div className="box-border w-[25px] h-[25px] border-[1px] border-dashed border-darkslateblue-100 justify-center flex flex-row items-center">
                                        <div className="leading-[100%] font-medium">
                                        -
                                        </div>
                                    </div>
                                </div>
                                <div className="text-xs leading-[100%] font-medium ml-[20px]">
                                    <p className="mt-1.5">Diskon</p>
                                </div>
                            </div>
                            <b className="leading-[100%] text-red-500 text-right">
                               - Rp 100.000
                            </b>
                        </div>
                        }
                    </div>
                    <div className="absolute ml-[75px] mt-[80px] flex flex-row w-[420px] border-[1px] border-dashed border-darkslateblue-100"/>
                    <div className="w-[426px] h-[41px] mt-[105px] ml-[70px] text-right text-base">
                        <div className="flex flex-row justify-between items-center">
                            <div className="w-[45px] h-[40.85px]">
                                <Image src={selectPayment.icon}/>
                            </div>
                            <div>
                                <b className="leading-[100%]">
                                Total<br/>
                                <b className="text-xl leading-[100%] text-orange">
                                Rp 3.500.000
                                </b>
                                </b>
                            </div>
                        </div>
                        <div style={{marginLeft: isPayment == false ? "110px" : "165px"}} className="flex flex-row mt-7">
                            <div  className="rounded-3xl border-[2px] border-solid hover:border-darkslateblue-100 bg-darkslateblue-100 hover:bg-transparent py-2.5 px-5 text-white hover:text-darkslateblue-100 " onClick={() => {isPayment == false ? setIsPayment(true) : setModalPayment({state: 1})}}>
                                <div className="leading-[140%] font-medium">
                                    {isPayment == false ? "Continue to Payment" : "Chekout"}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="mt-[180px]"/>
            <Footer_2/>
            {modalPayment.state == 2 &&
            <div className="justify-center items-center flex overflow-x-hidden bg-black bg-opacity-50 overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
                <div className="flex items-start justify-between  p-5 border-b border-solid border-transparent rounded-t">
                    <div className="rounded-[50px] w-full h-[650px] overflow-hidden text-center text-3xs text-darkslateblue font-montserrat">
                    <div className="absolute top-[calc(50%_-_293px)] mt-10 left-[calc(50%_-_190px)] w-[380px] h-[585px]">
                        <img
                        className="absolute top-[28px] left-[-27px] w-[434px] h-[590px]"
                        alt=""
                        src="/subtract.svg"
                        />
                        <img
                        className="absolute top-[-12px] left-[143px] w-[94px] h-[94px]"
                        alt=""
                        src="/group-37.svg"
                        />
                        <div className="absolute top-[95px] left-[calc(50%_-_160px)] w-80 h-[77px] text-[20px]">
                            <b className="absolute top-[0px] left-[calc(50%_-_95px)] leading-[100%] text-darkslateblue-100">
                                Payment Success!
                            </b>
                            <div className="absolute top-[35px] left-[calc(50%_-_160px)] text-[14px]  text-darkslateblue-100 text-sm leading-[100%] inline-block w-80">{`We’ve sent you an email with all details of your order & remember you can track your order using this web!`}</div>
                            </div>
                            <div className="absolute top-[190px] w-[300px] left-[calc(53%_-_160px)] border-darkslateblue-100 border-solid border-[0.5px]"/>
                            <div className="absolute top-[212px] left-[calc(50%_-_51px)]  text-darkslateblue-100  text-sm leading-[100%] font-medium">
                            Total Payment
                            </div>
                        {/* <div className="absolute top-[191.5px] left-[calc(50%_-_160.5px)] box-border w-[321px] h-px border-t-[1px] border-solid border-darkslateblue" /> */}
                        <div className="absolute top-[295px] left-[30px] w-[150px] h-[59px] text-left">
                        <div className="absolute top-[0px] left-[0px] rounded-3xs bg-gray-200 box-border w-[150px] h-[59px] border-[1px] rounded-[10px] border-solid border-darkslateblue-100" />
                        <div className="absolute top-[15px] left-[15px] w-[95px] h-[29px]">
                            <div className="absolute text-[10px] top-[0px] left-[0px] leading-[100%]">
                            Order ID
                            </div>
                            <div className="absolute top-[17px] left-[0px] text-xs leading-[100%] font-semibold">
                            072023001
                            </div>
                        </div>
                        </div>
                        <div className="absolute top-[295px] left-[200px] w-[150px] h-[59px] text-left">
                        <div className="absolute top-[0px] bg-gray-200 left-[0px] rounded-3xs bg-gainsboro box-border w-[150px] h-[59px] border-[1px] rounded-[10px] border-solid border-darkslateblue-100" />
                        <div className="absolute top-[15px] left-[15px] w-[127px] h-[29px]">
                            <div className="absolute text-[10px] top-[0px] left-[0px] leading-[100%]">
                            Payment Time
                            </div>
                            <div className="absolute top-[17px] left-[0px] text-xs leading-[100%] font-semibold">
                            15 Juni 2023, 11:03
                            </div>
                        </div>
                        </div>
                        <div className="absolute top-[379px] left-[30px] w-[150px] h-[59px] text-left">
                        <div className="absolute top-[0px] bg-gray-200 left-[0px] rounded-3xs bg-gainsboro box-border w-[150px] h-[59px] border-[1px] rounded-[10px] border-solid border-darkslateblue-100" />
                        <div className="absolute top-[15px] left-[15px] w-[128px] h-[29px]">
                            <div className="absolute text-[10px] top-[0px] left-[0px] leading-[100%]">
                            Payment Method
                            </div>
                            <div className="absolute top-[17px] left-[0px] text-xs leading-[100%] font-semibold">
                            Bank Transfer
                            </div>
                        </div>
                        </div>
                        <div className="absolute top-[379px] left-[200px] w-[150px] h-[59px] text-left">
                        <div className="absolute top-[0px] bg-gray-200 left-[0px] rounded-3xs bg-gainsboro box-border w-[150px] h-[59px] border-[1px] rounded-[10px] border-solid border-darkslateblue-100" />
                        <div className="absolute top-[15px] left-[15px] w-[121px] h-[29px]">
                            <div className="absolute text-[10px] top-[0px] left-[0px] leading-[100%]">
                            Customer Details
                            </div>
                            <div className="absolute top-[17px] w-[150px] left-[0px] text-xs leading-[100%] font-semibold">
                            {name}
                            </div>
                        </div>
                        </div>
                        <b className="absolute top-[236px] left-[calc(50%_-_83px)] text-[24px] leading-[100%] text-orange">
                        Rp 3.400.000
                        </b>
                        <div className="absolute border-[2px] border-solid hover:border-darkslateblue-100 bg-darkslateblue-100 hover:bg-white top-[478px] left-[calc(50%_-_72px)] rounded-3xl bg-darkslateblue flex flex-row py-[15px] px-5 items-start justify-start text-sm text-white hover:text-darkslateblue-100" onClick={() => router.push("/page/LandingPages")}>
                            <div className="relative leading-[140%] font-medium">
                                Back to home!
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            }
            {modalPayment.state == 1 &&
            <div className="justify-center items-center flex overflow-x-hidden bg-black bg-opacity-50 overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
                <div className="w-[500px] mt-20 h-[628px] bg-white">
                    <div className="w-full h-[70px] flex flex-row justify-between items-center bg-darkslateblue-100">
                        <div className="ml-5 text-[16px] text-white top-[0px] left-[0px] leading-[100%]">
                            Total Harga
                        </div>
                        <b className="mr-5 text-[16px] text-white top-[0px] left-[0px] leading-[100%]">
                            RP 3.500.000
                        </b>
                    </div>
                    <Image src={IC_BCA_2} className="ml-5 mt-7"/>
                    <div className="w-[458px] my-5 ml-5 h-[0.5px] border-solid border-gray-300 border-[0.5px]"/>
                    <div className="ml-5 text-[14px] text-black top-[0px] left-[0px] leading-[100%]">
                        No. Rekening
                    </div>
                    <div className="w-full h-[40px] flex flex-row justify-between items-center ">
                        <b className="ml-5 text-[18px] text-yellow-300 top-[0px] left-[0px] leading-[100%]">
                            1890 8967 7276 323
                        </b>
                        <div className="mr-5 text-[16px] text-black top-[0px] left-[0px] leading-[100%]">
                            SALIN
                        </div>
                    </div>
                    <div className="w-[458px] mb-3 mt-2 ml-5 h-[0.5px] border-solid border-gray-300 border-[0.5px]"/>
                    <div className="ml-5 text-[12px] mb-3 text-darkslateblue-100 top-[0px] left-[0px] leading-[100%]">
                        Proses verifikasi kurang dari 10 menit setelah pembayaran berhasil
                    </div>
                    <div className="ml-5 text-[12px] mb-5 text-black top-[0px] left-[0px] leading-[100%]">
                    Bayar pesanan ke Virtual Account di atas sebelum membuat pesanan<br/>kembali dengan Virtual Account agar nomor tetap sama.
                    </div>
                    <div className="ml-5 text-[12px] text-black top-[0px] left-[0px] leading-[100%]">
                    Hanya Menerima dari Bank BCA
                    </div>
                    <div className="w-[500px] h-3 my-6 bg-gray-300"/>
                    {GuidePayment.map(x => {
                        return (
                            <div>
                                <div className="w-full h-[37px] justify-between items-center flex flex-row">
                                    <div className="ml-5 text-[14px] text-black top-[0px] left-[0px] leading-[100%]">
                                        No. Rekening
                                    </div>
                                    <Image src={IC_DROP_DOWN} className="mr-7"/>
                                </div>
                                <div className="w-[500px] h-[1px] my-2 bg-gray-300"/>
                            </div>
                        )
                    })}
                    <div className="w-[150px] ml-[170px] rounded-3xl mt-5 border-[2px] border-solid hover:border-darkslateblue-100 bg-darkslateblue-100 hover:bg-white h-10 flex flex-row py-2.5 px-[15px] box-border items-center justify-center text-center hover:text-darkslateblue-100 text-white" onClick={() => setModalPayment({state: 2})}>
                        <div className="font-medium">Konfirmasi</div>
                    </div>
                </div>
            </div>
            }
        </div>
    )
}

export default OrderDetails