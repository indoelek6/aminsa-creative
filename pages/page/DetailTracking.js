import Image from "next/image"
import { useState } from "react"
import Footer_2 from "../footer_2"
import Navbar from "../navbar"
import IC_BANK from "../../public/IC_BANK.png"
import IC_BCA from "../../public/IC_BCA.png"
import IC_VISA from "../../public/IC_VISA.png"
import IC_WALLET from "../../public/IC_WALLET.png"
import IC_BCA_2 from "../../public/IC_BCA_2.svg"
import IC_DROP_DOWN from "../../public/IC_DROP_DOWN.svg"
import { useRouter } from "next/router"


const PaymentList = [
    {
        icon: IC_VISA,
        name: "Credit/Debit Cards"
    },
    {
        icon: IC_BCA,
        name: "BCA Virtual Account"
    },
    {
        icon: IC_BANK,
        name: "ATM/Bank Transfer"
    },
    {
        icon: IC_WALLET,
        name: "Bank Center"
    },
]

const Guide = [
    {
        name: "Project Inception",
        desc: "Analyze Brand Image"
    },
    {
        name: "Research",
        desc: "Conduct Research About Brand Past Social Media Activities"
    },
    {
        name: "Update",
        desc: "Updating Content as Per Engagement Calender"
    },
    {
        name: "Report Results",
        desc: "Attach the Results of Reports From Social Media Analytics"
    },
]

const DetailTracking = () => {

    const router = useRouter()

    const [isPayment,setIsPayment] = useState(false)
    const [modalPayment, setModalPayment] = useState({
        state: 0
    })

    return (
        <div className="bg-thistle w-full overflow-y-auto font-heading-5-500">
            <Navbar isNav/>
            <div className="top-[0px] left-[0px] bg-darkslateblue-100 w-[1280px]" />
            <div className="bg-darkslateblue-100 w-full h-[100px] overflow-hidden text-center text-20xl text-white"/>
            <div className="mt-24"/>
            <div className="flex flex-row mt-5 justify-between px-24">
                <div>
                    <b className="text-[30px] leading-[100%] text-darkslateblue">
                    Your Post-Production<br/>Progress
                    </b>
                    <div className="flex mt-16 flex-row items-center">
                        <div className="mr-5">
                            <div className="w-[313px] rounded-lg h-[15px] bg-gray-400">
                                <div className="w-[113px] rounded-lg h-[15px] bg-yellow-500">

                                </div>
                            </div>
                        </div>
                        <b className="text-[25px] leading-[100%] text-darkslateblue-100">
                        25%
                        </b>
                    </div>
                    <div className="mt-16"/>
                    {Guide.map((x, i) => {
                        return (
                            <>
                                <div className="flex flex-row justify-start">
                                    <div className="w-[30px] h-[30px] mr-5 bg-gray-400 bg-opacity-50 rounded-full flex flex-row justify-center items-center">
                                        {i == 0 ?
                                        <div className="w-[20px] h-[20px] bg-yellow-400 rounded-full"/>
                                        :
                                        <div className="w-[20px] h-[20px] bg-gray-400 rounded-full"/>
                                        }
                                    </div>
                                    <div>
                                        <b className="text-[24px] leading-[100%] text-darkslateblue">
                                        {x.name}
                                        </b>
                                        <div className="text-[12px] mt-2 leading-[100%] text-darkslateblue">
                                        {x.desc}
                                        </div>
                                    </div>
                                </div>
                                {i !== 3 &&
                                <div className="w-[0] h-12 border-[1px] mt-[-20px] border-dashed ml-3.5 border-darkslateblue-100"/>
                                }
                            </>
                        )
                    })}
                </div>
                <div className="w-[520px] h-[768px] text-center text-sm text-darkslateblue-100">
                    <img
                    className="w-[574px] h-[822px]"
                    alt=""
                    src="/subtract.svg"
                    />
                    <div className="mt-[-780px] ml-[70px] ">
                        <div className="w-[277.33px] h-[104px] text-[34px]">
                            <div className="ml-[75px]">
                                <b className="leading-[100%] inline-block text-[35px] w-[277.33px] h-[68px]">
                                    <p className="m-0">Social Climber</p>
                                    <p className="m-0">Package</p>
                                </b>
                                <div className="text-base leading-[100%] font-medium text-orange inline-block w-[277.33px] h-4">
                                    Order ID : 072023001
                                </div>
                            </div>
                            <div className="absolute my-5 flex flex-row w-[420px] border-[1px] border-dashed border-darkslateblue-100"/>
                            <div className="w-[260px] h-[97px] text-left mt-8">
                                <b className="text-[20px] leading-[100%] inline-block w-[197.17px] h-5 mb-5">
                                    Customer Details
                                </b>
                                <div className="leading-[100%] text-[14px] w-[260px] h-3.5 mb-2">
                                    Name : Muhammad Rezvin Akbar
                                </div>
                                <div className="leading-[100%] text-[14px] w-[236.17px] h-3.5 mb-2">
                                    Email : rezvin.apin@gmail.com
                                </div>
                                <div className="leading-[%] text-[14px] w-[172.25px] h-3.5 mb-2">
                                    Phone : 089677276323
                                </div>
                            </div>
                            <div className="absolute my-5 mt-[40px] flex flex-row w-[420px] border-[1px] border-dashed border-darkslateblue-100"/>
                    </div>
                    </div>
                    <div className="w-[334px] mt-[200px] h-[65.85px] text-xl">
                        <b className="leading-[100%] ml-[220px] inline-block w-[139px] h-[18.46px]">
                            Order Details
                        </b>
                        <div className="flex flex-row ml-[135px] mt-3 items-start justify-start gap-[5px] text-xs">
                            <div className="rounded-xl flex flex-row py-2.5 px-1 items-start justify-start border-[2px] border-solid border-darkslateblue">
                                <div className="leading-[100%] min-w-[80px] font-medium">
                                    30 IG Feeds
                                </div>
                            </div>
                            <div className="rounded-xl flex flex-row py-2.5 px-1 items-start justify-start border-[2px] border-solid border-darkslateblue">
                                <div className="leading-[100%] min-w-[80px] font-medium">
                                    15 IG Stories
                                </div>
                            </div>
                            <div className="rounded-xl flex flex-row py-2.5 px-5 items-start justify-start border-[2px] border-solid border-darkslateblue">
                                <div className="leading-[100%] min-w-[60px] font-medium">
                                    5 IG Reels
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="w-[420px] ml-[70px] h-16 text-left mt-[35px]">
                        <div className="flex flex-row justify-between items-center">
                            <div className="leading-[100%] font-medium">
                                Booking Date:
                            </div>
                            <b className="leading-[100%] text-right">
                                15 Juni 2023
                            </b>
                        </div>
                        <div className="flex flex-row justify-between mt-[20px] items-center">
                            <div className="flex flex-row">
                                <div className="w-[25px] h-[25px]">
                                    <div className="box-border w-[25px] h-[25px] border-[1px] border-dashed border-darkslateblue-100 justify-center flex flex-row items-center">
                                        <div className="leading-[100%] font-medium">
                                        1
                                        </div>
                                    </div>
                                </div>
                                <div className="text-xs leading-[100%] font-medium ml-[20px]">
                                    <p className="m-0">Social Climber</p>
                                    <p className="m-0">Package</p>
                                </div>
                            </div>
                            <b className="leading-[100%] text-orange text-right">
                                Rp 3.500.000
                            </b>
                        </div>
                        <div className="flex flex-row justify-between mt-[20px] items-center">
                            <div className="flex flex-row">
                                <div className="w-[25px] h-[25px]">
                                    <div className="box-border w-[25px] h-[25px] border-[1px] border-dashed border-darkslateblue-100 justify-center flex flex-row items-center">
                                        <div className="leading-[100%] font-medium">
                                        -
                                        </div>
                                    </div>
                                </div>
                                <div className="text-xs leading-[100%] font-medium ml-[20px]">
                                    <p className="mt-1.5">Diskon</p>
                                </div>
                            </div>
                            <b className="leading-[100%] text-red-500 text-right">
                               - Rp 100.000
                            </b>
                        </div>
                    </div>
                    <div className="absolute ml-[75px] mt-[80px] flex flex-row w-[420px] border-[1px] border-dashed border-darkslateblue-100"/>
                    <div className="w-[426px] h-[41px] mt-[105px] ml-[70px] text-right text-base">
                        <div className="flex flex-row justify-between items-center">
                            <div className="w-[45px] h-[40.85px]">
                                <img
                                className="w-[20.77px] h-[20.77px] object-cover"
                                alt=""
                                src="/image-4@2x.png"
                                />
                                <img
                                className="w-[20.77px] h-[20.77px] object-cover"
                                alt=""
                                src="/image-3@2x.png"
                                />
                                <img
                                className="w-[45px] h-[16.62px] object-cover"
                                alt=""
                                src="/image-5@2x.png"
                                />
                            </div>
                            <div>
                                <b className="leading-[100%]">
                                Total<br/>
                                <b className="text-xl leading-[100%] text-orange">
                                Rp 3.500.000
                                </b>
                                </b>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="mt-[180px]"/>
            <Footer_2/>    
        </div>
    )
}

export default DetailTracking