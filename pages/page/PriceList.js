import { useState } from "react"
import Footer from "../footer"
import Footer_2 from "../footer_2"
import Navbar from "../navbar"
import { useRouter } from "next/router"

const ListItem = [
    {   
        img: '/rectangle-29@2x.png',
        name: 'Social Media Management',
        price: "Rp 1.000.000",
        desc_name: 'A lot of work and love goes into all the accounts we manage on social media. We specialize in strategizing, designing and organically growing social media accounts on Instagram, Facebook, TikTok, LinkedIn and Pinterest.',
        tagging: '#Social Media Management #Content Planning #Creative Manangement #Product Photography'
    },
    {
        img: '/rectangle-291@2x.png',
        name: 'Product Photography',
        price: "Rp 3.500.000",
        desc_name: 'Showcasing the beauty of our product through captivating imagery. Our product photography captures every detail, highlighting its elegance and functionality. Get ready to be inspired and enticed by the artistry behind our stunning visuals.',
        tagging: '#Social Media Management #Content Creative #Content Planning #Content Management #Ads Production'
    },
]

const PriceList = () => {

    const router = useRouter()

    const [show,setShow] = useState("")

    return (
        <div className="relative bg-thistle w-full overflow-y-auto text-left text-xl font-heading-5-500">
            <Navbar isNav/>
            <div className="top-[0px] left-[0px] bg-darkslateblue-100 w-[1280px]" />
            <div className="bg-darkslateblue-100 w-full h-[100px] overflow-hidden text-center text-20xl text-white"/>
            <div className="w-full flex mt-20 flex-col items-center justify-start gap-[20px] text-center">
                <b className="relative leading-[140%] text-[39px] flex items-center justify-center w-[614px] h-[46px] shrink-0">
                Our Services
                </b>
                <div className="text-xl leading-[140%] inline-block w-[1080px] h-[98px] shrink-0">
                Aminsa Creative House is here to help your brand stand out among other<br/>competitors. Each service is your brand's step towards success.
                </div>
            </div>
            <div className="gap-[48px] grid grid-cols-1 self-center mt-[68px] left-[140px]">
            {ListItem.map((x) => {
                return (
                    <div className="ml-[180px] rounded-tl-[300px] rounded-tr-[30px] rounded-br-[30px] rounded-bl-[300px] bg-white shadow-[0px_20px_27px_rgba(0,_0,_0,_0.1)] w-[1080px] h-[450px] flex flex-row justify-center items-center content-center">
                        <img
                            className=" top-[40px] left-[40px] rounded-tl-281xl rounded-tr-xl rounded-br-xl rounded-bl-281xl w-[490px] h-[370px] object-cover"
                            alt=""
                            src={x.img}
                        />
                        <div className="ml-7">
                            <div className="flex flex-col items-start justify-start gap-[20px]">
                                <b className="leading-[140%] inline-block w-[470px]">
                                    {x.name}
                                </b>
                                <div className="flex flex-row items-start justify-start gap-[10px] text-5xl text-orange">
                                <div className="leading-[140%]">Starts from</div>
                                <b className="leading-[140%]">{x.price}</b>
                                </div>
                                <div className="text-base leading-[140%] flex items-center w-[470px]">
                                {x.desc_name}
                                </div>
                            </div>
                            <div onClick={() => router.push("/page/DetailPriceList")} className="rounded-3xl mt-16 bg-darkslateblue-100 hover:bg-transparent hover:border-[2px] hover:text-darkslateblue-100 border-solid hover:border-darkslateblue-100 w-[110px] flex flex-row py-4 px-[25px] items-start justify-start text-center text-xl text-white">
                                <div className="leading-[140%] font-medium">
                                See more...
                                </div>
                            </div>
                        </div>
                        </div>
                )
            })}
            </div>
            <div className="mt-28"/>
            <Footer_2/>
        </div>
    )
}

export default PriceList