/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      outlineWidth: {
        0: '0px'
      },
      colors: {
        white: "#fff",
        darkslateblue: {
          "100": "#312b63",
          "200": "rgba(49, 43, 99, 0.76)",
          "300": "rgba(49, 43, 99, 0)",
        },
        orange: "#ffba00",
        salmon: "#fc6666",
        darkgray: "#a0a0a0",
        thistle: "#f4d4f1",
      },
      fontFamily: {
        "heading-5-500": "Montserrat",
        inherit: "inherit",
      },
      borderRadius: {
        "11xl": "30px",
      },
    },
    fontSize: {
      xl: "20px",
      "6xl": "25px",
      base: "16px",
      xs: "12px",
      "9xl": "28px",
      "20xl": "39px",
    },
  },
  corePlugins: {
    preflight: false,
  },
};
